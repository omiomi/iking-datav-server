package com.ikingtech.platform.datav.template.model;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ikingtech.framework.sdk.base.model.PageResult;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.BaseEntity;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.platform.datav.template.entity.TemplateDO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Objects;

/**
 * @author fucb
 */

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@Schema(name = "TemplateDTO对象", description = "对象")
public class TemplateDTO extends BaseEntity {

    /**
     * 描述
     */
    @Schema(name = "description", description = "描述")
    private String description;

    /**
     * 配置
     */
    @Schema(name = "config", description = "配置")
    private String config;

    /**
     * 名称
     */
    @Schema(name = "name", description = "名称")
    private String name;

    /**
     * 宽度
     */
    @Schema(name = "width", description = "宽度")
    private Integer width;

    /**
     * 高度
     */
    @Schema(name = "height", description = "高度")
    private Integer height;

    /**
     * 快照
     */
    @Schema(name = "snapshot", description = "快照")
    private String snapshot;

    /**
     * 组件
     */
    @Schema(name = "coms", description = "组件")
    private String coms;

    /**
     * 描述
     */
    @Schema(name = "isSystem", description = "描述")
    private Boolean isSystem;

    /**
     * 用户ID
     */
    @Schema(name = "userId", description = "用户ID")
    private String userId;

}

