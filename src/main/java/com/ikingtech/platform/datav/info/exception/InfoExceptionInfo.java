package com.ikingtech.platform.datav.info.exception;

import com.ikingtech.framework.sdk.context.exception.FrameworkExceptionInfo;
import lombok.RequiredArgsConstructor;

/**
 * @author fucb
 */

@RequiredArgsConstructor
public enum InfoExceptionInfo implements FrameworkExceptionInfo {

    /**
     * 基础信息不存在
     */
    INFO_NOT_FOUND("infoNotFound"),
    ;

    private final String message;

    @Override
    public String message() {
        return this.message;
    }

    @Override
    public String moduleName() {
        return "iking-framework-maintenace-datav";
    }
}
