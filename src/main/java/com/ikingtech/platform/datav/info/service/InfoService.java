package com.ikingtech.platform.datav.info.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fasterxml.jackson.core.type.TypeReference;
import com.ikingtech.framework.sdk.base.model.PageResult;
import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import com.ikingtech.framework.sdk.context.security.Me;
import com.ikingtech.framework.sdk.department.api.DeptApi;
import com.ikingtech.framework.sdk.department.model.DeptBasicDTO;
import com.ikingtech.framework.sdk.department.model.DeptQueryParamDTO;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.platform.datav.component.entity.ComponentDO;
import com.ikingtech.platform.datav.component.entity.TemplateComponent;
import com.ikingtech.platform.datav.component.service.ComponentService;
import com.ikingtech.platform.datav.component.service.TemplateComponentService;
import com.ikingtech.platform.datav.filter.entity.FilterDO;
import com.ikingtech.platform.datav.filter.model.enums.FilterTypeEnum;
import com.ikingtech.platform.datav.filter.service.FilterService;
import com.ikingtech.platform.datav.info.entity.InfoDO;
import com.ikingtech.platform.datav.info.exception.InfoExceptionInfo;
import com.ikingtech.platform.datav.info.mapper.InfoMapper;
import com.ikingtech.platform.datav.info.model.InfoDTO;
import com.ikingtech.platform.datav.info.model.InfoSearchDTO;
import com.ikingtech.platform.datav.info.model.InfoVO;
import com.ikingtech.platform.datav.template.model.TemplateVO;
import com.ikingtech.platform.datav.template.service.TemplateService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author fucb
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class InfoService extends ServiceImpl<InfoMapper, InfoDO> {

    private final DeptApi deptApi;

    private final ComponentService componentService;

    private final TemplateService templateService;

    private final TemplateComponentService templateComponentService;

    private final FilterService filterService;

    public PageResult<InfoVO> selectByPage(InfoSearchDTO queryParam) {
        List<String> list;
        if (Me.isAdmin()) {
            DeptQueryParamDTO dto = new DeptQueryParamDTO();
            dto.setParentDeptId(queryParam.getGroupId());
            list = deptApi.listSubInfoAll(dto).getData().stream().map(DeptBasicDTO::getId).collect(Collectors.toList());
        } else {
            list = Me.dataScope(queryParam.getGroupId());
        }
        list.add(queryParam.getGroupId());

        Page<InfoDO> page = this.page(new Page<>(queryParam.getPage(), queryParam.getRows()), Wrappers.<InfoDO>lambdaQuery()
                .in(Tools.Coll.isNotBlank(list), InfoDO::getGroupId, list)
                .like(Tools.Str.isNotBlank(queryParam.getName()), InfoDO::getName, queryParam.getName())
                .last(CharSequenceUtil.isNotBlank(queryParam.getOrder()), CharSequenceUtil.toUnderlineCase(Tools.Str.join("order by ", " desc", queryParam.getOrder()))));

        PageResult<InfoDO> pageResult = PageResult.build(page);
        return pageResult.convert(this::convertVO);
    }

    public InfoVO getInfo(String id) {
        InfoDO entity = this.baseMapper.selectById(id);
        if (null == entity) {
            throw new FrameworkException(InfoExceptionInfo.INFO_NOT_FOUND);
        }
        return this.convertVO(entity);
    }

    private InfoVO convertVO(InfoDO entity) {
        InfoVO infoVO = Tools.Bean.copy(entity, InfoVO.class);
        infoVO.setDialogs(Tools.Json.toBean(entity.getDialogs(), new TypeReference<>() {
        }));
        infoVO.setEvents(Tools.Json.toBean(entity.getEvents(), new TypeReference<>() {
        }));
        infoVO.setHost(Tools.Json.toBean(entity.getHost(), new TypeReference<>() {
        }));
        infoVO.setIframe(Tools.Json.toBean(entity.getIframe(), new TypeReference<>() {
        }));
        infoVO.setPages(Tools.Json.toBean(entity.getPages(), new TypeReference<>() {
        }));
        infoVO.setStyleFilterParams(Tools.Json.toBean(entity.getStyleFilterParams(), new TypeReference<>() {
        }));
        infoVO.setVariables(Tools.Json.toBean(entity.getVariables(), new TypeReference<>() {
        }));
        infoVO.setAddress(Tools.Json.toBean(entity.getAddress(), new TypeReference<>() {
        }));
        return infoVO;
    }

    @Transactional(rollbackFor = Exception.class)
    public InfoDO add(InfoDTO param) {
        InfoDO info = this.supply(param);
        String uuid = IdUtil.simpleUUID();
        info.setId(uuid);
        info.setShareUrl(this.getShareUrl(uuid));
        this.baseMapper.insert(info);
        return info;
    }

    private String getShareUrl(String uuid) {
        return "/share/#/screen/" + uuid;
    }

    public void add(InfoDO param) {
        this.baseMapper.insert(param);
    }

    public void edit(InfoDTO param) {
        InfoDO supply = this.supply(param);
        if (!this.exist(param.getId())) {
            throw new FrameworkException(InfoExceptionInfo.INFO_NOT_FOUND);
        }
        this.baseMapper.updateById(supply);
    }

    @Transactional(rollbackFor = Exception.class)
    public void delete(String id) {
        this.baseMapper.deleteById(id);
    }

    public boolean exist(String id) {
        return this.baseMapper.exists(Wrappers.<InfoDO>lambdaQuery().eq(InfoDO::getId, id));
    }

    @Transactional(rollbackFor = Exception.class)
    public void copy(InfoDTO param) {
        InfoDO byId = this.getById(param.getId());
        if (byId == null) {
            throw new FrameworkException(InfoExceptionInfo.INFO_NOT_FOUND);
        }
        String simpleUuid = IdUtil.simpleUUID();
        byId.setId(simpleUuid);
        byId.setName(byId.getName() + "副本");
        byId.setShared(false);
        byId.setShareUrl(null);
        byId.setSharePassword(null);

        HashMap<String, String> componentsMap = new HashMap<>(4);
        List<ComponentDO> componentDOList = componentService.getListByScreenId(param.getId());
        if (!componentDOList.isEmpty()) {
            componentDOList.forEach(e -> {
                String componentId = IdUtil.simpleUUID();
                componentsMap.put(e.getId(), componentId);
                e.setId(componentId);
                e.setScreenId(simpleUuid);
            });
            componentService.saveBatch(componentDOList);

            String pages = byId.getPages();
            if(Tools.Str.isNotBlank(pages)){
                List<HashMap<String, Object>> list = Tools.Json.toBean(pages, new TypeReference<>() {
                });
                list.forEach(e -> {
                    List<String> children = Tools.Json.objToBean(e.get("children"), new TypeReference<>() {
                    });
                    e.put("children", children.stream().map(componentsMap::get).collect(Collectors.toList()));
                });
                byId.setPages(Tools.Json.toJsonStr(list));
            }
        }
        this.baseMapper.insert(byId);

        List<FilterDO> filterDOList = filterService.getListByScreenId(param.getId());
        if (!filterDOList.isEmpty()) {
            filterDOList.forEach(e -> {
                e.setScreenId(simpleUuid);
                e.setId(IdUtil.simpleUUID());
            });
            filterService.saveBatch(filterDOList);
        }
    }

    public InfoDO supply(InfoDTO param) {
        InfoDO info = new InfoDO();
        info.setId(param.getId());
        info.setScreenId(param.getScreenId());
        info.setName(param.getName());
        info.setGroupId(param.getGroupId());
        info.setTemplateId(param.getTemplateId());
        info.setShareUrl(param.getShareUrl());
        info.setSharePassword(param.getSharePassword());

        info.setShareTime(param.getShareTime());
        info.setShared(param.getShared());
        info.setWidth(param.getWidth());
        info.setHeight(param.getHeight());
        info.setBgimage(param.getBgimage());
        info.setBgcolor(param.getBgcolor());
        info.setGrid(param.getGrid());
        info.setScreenshot(param.getScreenshot());
        info.setUseWatermark(param.getUseWatermark());
        info.setThumbnail(param.getThumbnail());
        info.setZoomMode(param.getZoomMode());
        info.setDefaultPage(param.getDefaultPage());

        info.setDialogs(Tools.Json.toJsonStr(param.getDialogs()));
        info.setEvents(Tools.Json.toJsonStr(param.getEvents()));
        info.setHost(Tools.Json.toJsonStr(param.getHost()));
        info.setIframe(Tools.Json.toJsonStr(param.getIframe()));
        info.setPages(Tools.Json.toJsonStr(param.getPages()));
        info.setStyleFilterParams(Tools.Json.toJsonStr(param.getStyleFilterParams()));
        info.setVariables(Tools.Json.toJsonStr(param.getVariables()));
        return info;
    }

    @Transactional(rollbackFor = Exception.class)
    public InfoDO create(InfoDTO param) {

        // 获取模板基本信息
        TemplateVO template = templateService.getInfo(param.getTemplateId());

        InfoDO infoDO = JSONUtil.toBean(template.getConfig(), InfoDO.class);
        String uuid = IdUtil.simpleUUID();
        infoDO.setId(uuid);
        infoDO.setGroupId(param.getGroupId());
        infoDO.setName(param.getName());
        infoDO.setShareUrl(this.getShareUrl(uuid));

        List<HashMap<String, Object>> list = Tools.Json.toBean(infoDO.getPages(), new TypeReference<>() {
        });
        if(Tools.Coll.isNotBlank(list)){
            list.forEach(e -> {
                List<String> children = Tools.Json.objToBean(e.get("children"), new TypeReference<>() {
                });

                List<TemplateComponent> templateComponents = templateComponentService.getComponent(children);
                List<ComponentDO> componentList = templateComponents.stream().map(component -> {
                    ComponentDO componentDO = BeanUtil.copyProperties(component, ComponentDO.class);
                    componentDO.setScreenId(uuid);
                    componentDO.setId(Tools.Str.removePrefix(componentDO.getName(), "V") + "_" + RandomUtil.randomString(9));
                    return componentDO;
                }).toList();
                List<String> componentIds = componentList.stream().map(ComponentDO::getId).toList();
                e.put("children", componentIds);
                componentService.saveBatch(componentList);
            });
        }
        infoDO.setPages(Tools.Json.toJsonStr(list));
        this.save(infoDO);

        List<FilterDO> filterDOList = filterService.getListByTemplateId(param.getTemplateId());
        if (!filterDOList.isEmpty()) {
            filterDOList.forEach(e -> {
                e.setTemplateId(null);
                e.setScreenId(uuid);
                e.setId(IdUtil.simpleUUID());
                e.setFilterType(FilterTypeEnum.COMMON.name());
            });
            filterService.saveBatch(filterDOList);
        }
        return infoDO;
    }

    public void share(InfoDTO param) {
        InfoDO byId = this.getById(param.getId());
        if (byId == null) {
            throw new FrameworkException(InfoExceptionInfo.INFO_NOT_FOUND);
        }
        byId.setShared(param.getShared());
        byId.setShareUrl(param.getShareUrl());
        byId.setSharePassword(param.getSharePassword());
        this.baseMapper.updateById(byId);
    }
}
