package com.ikingtech.platform.datav.component.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ikingtech.framework.sdk.data.mybatisplus.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import software.amazon.awssdk.services.s3.endpoints.internal.Value;

/**
 * 组件表(ComponentDO)实体类
 *
 * @author fucb
 * @since 2024-02-22 09:28:41
 */

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@TableName(value = "datav_component")
public class ComponentDO extends BaseEntity {

    /**
     * 大屏ID
     */
    @TableField("screen_id")
    private String screenId;

    /**
     * 排序序号
     */
    @TableField("sort_order")
    private Integer sortOrder;

    /**
     * 类型
     */
    @TableField("type")
    private String type;

    /**
     * 别名
     */
    @TableField("alias")
    private String alias;

    /**
     * 名称
     */
    @TableField("name")
    private String name;

    /**
     * 子组件
     */
    @TableField("children")
    private String children;

    /**
     * 是否锁定
     */
    @TableField("locked")
    private Boolean locked;

    /**
     * 父级ID
     */
    @TableField("parent_id")
    private String parentId;

    /**
     * 是否隐藏
     */
    @TableField("hided")
    private Boolean hided;

    /**
     * 图标
     */
    @TableField("icon")
    private String icon;

    /**
     * 图片
     */
    @TableField("img")
    private String img;

    /**
     * 属性
     */
    @TableField("attr")
    private String attr;

    /**
     * 配置
     */
    @TableField("config")
    private String config;

    /**
     * API配置
     */
    @TableField("apis")
    private String apis;

    /**
     * API数据
     */
    @TableField("api_data")
    private String apiData;

    /**
     * 入场动画
     */
    @TableField("animate")
    private String animate;

    /**
     * 背景图
     */
    @TableField("bg_img")
    private String bgImg;

    /**
     * 组内配置
     */
    @TableField("scaling")
    private String scaling;

    /**
     * 事件
     */
    @TableField("events")
    private String events;

    /**
     * 是否选中
     */
    @TableField("selected")
    private Boolean selected;

    /**
     * 是否悬停
     */
    @TableField("hovered")
    private Boolean hovered;

    /**
     * 背景动画
     */
    @TableField("bg_animation")
    private String bgAnimation;

    /**
     * 弹窗配置
     */
    @TableField("dialog")
    private String dialog;

    /**
     * 是否为弹窗
     */
    @TableField("is_dialog")
    private Integer isDialog;

    /**
     * 版本
     */
    @TableField("version")
    private Integer version;

    /**
     * 是否为公共收藏
     */
    @TableField("is_public")
    private Integer isPublic;

    /**
     * 项目ID
     */
    @TableField("project_id")
    private String projectId;

    /**
     * 模板ID
     */
    @TableField("template_id")
    private String templateId;

    /**
     * 响应事件
     */
    @TableField("dis_actions")
    private String disActions;

    /**
     * 组件类型（普通组件，收藏组件，模板组件）
     */
    @TableField("component_type")
    private String componentType;

}

