package com.ikingtech.platform.datav.component.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ikingtech.framework.sdk.base.model.PageResult;
import com.ikingtech.framework.sdk.context.security.Me;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.web.annotation.ApiController;
import com.ikingtech.framework.sdk.web.annotation.GetRequest;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import com.ikingtech.platform.datav.component.entity.ComponentDO;
import com.ikingtech.platform.datav.component.enums.ComponentTypeEnum;
import com.ikingtech.platform.datav.component.model.ComponentDTO;
import com.ikingtech.platform.datav.component.model.ComponentSearchDTO;
import com.ikingtech.platform.datav.component.model.ComponentVO;
import com.ikingtech.platform.datav.component.service.ComponentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 组件表(ComponentDO)控制层
 *
 * @author fucb
 * @since 2024-02-22 09:28:34
 */
@RestController
@RequiredArgsConstructor
@ApiController(value = "/component", name = "可视化大屏-组件管理", description = "可视化大屏-组件管理")
public class ComponentController {

    private final ComponentService componentService;

    /**
     * 查询-个人收藏和公共收藏的组件
     *
     * @return 查询结果
     */
    @GetRequest(value = "/list", summary = "查询-个人收藏和公共收藏的组件", description = "查询-个人收藏和公共收藏的组件")
    public R<List<ComponentVO>> adminComComponentList() {
        LambdaQueryWrapper<ComponentDO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ComponentDO::getComponentType, ComponentTypeEnum.COLLECTED.name());
        queryWrapper.and(q -> q.eq(ComponentDO::getIsPublic, 1).or().eq(ComponentDO::getCreateBy, Me.id()));
        List<ComponentDO> list = componentService.list(queryWrapper);
        return R.ok(componentService.convertVO(list));
    }

    /**
     * 分页查询
     *
     * @param param 筛选条件
     * @return 查询结果
     */
    @PostRequest(value = "/select/page", summary = "查询-分页查询", description = "查询-分页查询(NORMAL-普通组件,COLLECTED-收藏组件,TEMPLATE-模板组件)")
    public R<List<ComponentVO>> selectByPage(@RequestBody ComponentSearchDTO param) {
        LambdaQueryWrapper<ComponentDO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ComponentDO::getComponentType, ComponentTypeEnum.COLLECTED.name());
        queryWrapper.eq(ComponentDO::getIsPublic, Me.isAdmin() ? 1 : 0);
        queryWrapper.eq(ComponentDO::getCreateBy, Me.id());
        Page<ComponentDO> page = componentService.page(new Page<>(param.getPage(), param.getRows()), queryWrapper);
        PageResult<ComponentVO> pageResult = PageResult.build(page.getPages(), page.getTotal(), componentService.convertVO(page.getRecords()));
        return R.ok(pageResult);
    }

    /**
     * 查询详情
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetRequest(value = "/get/info", summary = "查询详情", description = "查询详情")
    public R<ComponentVO> getInfo(@RequestParam("id") String id) {
        return R.ok(this.componentService.getInfo(id));
    }

    /**
     * 新增
     *
     * @param param 实体
     * @return 新增结果
     */
    @PostRequest(value = "/add", summary = "新增", description = "新增(NORMAL-普通组件,COLLECTED-收藏组件,TEMPLATE-模板组件)")
    public R<?> add(@RequestBody ComponentDTO param) {
        if (CharSequenceUtil.isBlank(param.getId())) {
            param.setId(IdUtil.simpleUUID());
        }
        this.componentService.add(param);
        Map<String, String> map = new HashMap<>(1);
        map.put("id", param.getId());
        return R.ok(map);
    }

    /**
     * 新增
     *
     * @param param 实体
     * @return 新增结果
     */
    @PostRequest(value = "/adds", summary = "新增s", description = "新增(NORMAL-普通组件,COLLECTED-收藏组件,TEMPLATE-模板组件)")
    public R<?> add(@RequestBody List<ComponentDTO> param) {
        List<ComponentDO> collect = param.stream().map(this.componentService::extracted).collect(Collectors.toList());
        this.componentService.saveBatch(collect);
        return R.ok();
    }

    /**
     * 编辑
     *
     * @param param 实体
     * @return 编辑结果
     */
    @PostRequest(value = "/edit", summary = "编辑", description = "编辑")
    public R<String> edit(@RequestBody ComponentDTO param) {
        this.componentService.edit(param);
        return R.ok();
    }

    /**
     * 删除
     *
     * @param list 主键
     * @return 删除是否成功
     */
    @PostRequest(value = "/del", summary = "删除", description = "删除")
    public R<String> delete(@RequestBody List<String> list) {
        this.componentService.removeBatchByIds(list);
        return R.ok();
    }

    @GetRequest(value = "/coms", summary = "查询-根据屏幕id获取组件列表", description = "根据屏幕id获取组件列表")
    public R<List<ComponentVO>> comS(@RequestParam String screenId) {
        return R.ok(componentService.convertVO(this.componentService.getListByScreenId(screenId)));
    }

    @PostRequest(value = "/updates", summary = "查询-保存大屏", description = "查询-保存大屏updates")
    public R<?> updates(@RequestBody List<ComponentDTO> componentDTOList) {
        if (componentDTOList.isEmpty()) {
            return R.ok();
        }
        List<ComponentDO> collect = componentDTOList.stream().map(componentService::extracted).collect(Collectors.toList());
        componentService.saveOrUpdateBatch(collect);
        return R.ok();
    }

    @PostRequest(value = "/delete/by/screen")
    public R<?> deleteByScreen(@RequestBody ComponentDTO param) {
        componentService.removeByScreenId(param.getId());
        return R.ok();
    }

    @PostRequest(value = "/collection")
    private R<?> collection(@RequestBody ComponentDTO param) {
        param.setId(IdUtil.simpleUUID());
        ComponentDO extracted = this.componentService.extracted(param);
        extracted.setCreateBy(Me.id());
        extracted.setComponentType(ComponentTypeEnum.COLLECTED.name());
        extracted.setIsPublic(Me.isAdmin() ? 1 : 0);
        this.componentService.save(extracted);
        return R.ok();
    }

    @PostRequest(value = "/delete/collection")
    public R<?> deleteCollection(@RequestBody ComponentDTO dto) {
        this.componentService.removeBatchByIds(dto.getIds());
        return R.ok();
    }

    private ComponentVO modelConvert(ComponentDO entity) {
        return BeanUtil.copyProperties(entity, ComponentVO.class);
    }

}

