package com.ikingtech.platform.datav.file.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

@Data
public class ProjectFileDeleteDTO {

    @Schema(name = "ids", description = "文件ID列表")
    private List<String> ids;
}
