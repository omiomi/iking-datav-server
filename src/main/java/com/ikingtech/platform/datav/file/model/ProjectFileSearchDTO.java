package com.ikingtech.platform.datav.file.model;

import com.ikingtech.framework.sdk.base.model.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author fucb
 */

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@Schema(name = "ProjectFileSearchDTO对象", description = "对象")
public class ProjectFileSearchDTO extends PageParam {


    /**
     * ID
     */
    @Schema(name = "id", description = "ID")
    private String id;

    /**
     * 用户ID
     */
    @Schema(name = "userId", description = "用户ID")
    private String userId;

    /**
     * 用户名称
     */
    @Schema(name = "userName", description = "用户名称")
    private String userName;

    /**
     * 文件名称
     */
    @Schema(name = "fileName", description = "文件名称")
    private String fileName;

    /**
     * 项目ID
     */
    @Schema(name = "projectId", description = "项目ID")
    private String projectId;

    /**
     * 文件类型
     */
    @Schema(name = "type", description = "文件类型")
    private ProjectFileTypeEnum type;

    /**
     * 文件路径
     */
    @Schema(name = "path", description = "文件路径")
    private String path;

    /**
     * 文件混淆名称
     */
    @Schema(name = "mixFileName", description = "文件混淆名称")
    private String mixFileName;

    @Schema(name = "fileType", description = "文件类型,用于检索 图片：IMAGE，视频：VIDEO，其他：OTHER")
    private String fileType;

}

