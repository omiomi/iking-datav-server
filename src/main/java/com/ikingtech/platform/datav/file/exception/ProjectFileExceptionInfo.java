package com.ikingtech.platform.datav.file.exception;

import com.ikingtech.framework.sdk.context.exception.FrameworkExceptionInfo;
import lombok.RequiredArgsConstructor;

/**
 * @author fucb
 */

@RequiredArgsConstructor
public enum ProjectFileExceptionInfo implements FrameworkExceptionInfo {

    /**
     * 项目文件不存在
     */
    PROJECT_FILE_NOT_FOUND("projectFileNotFound"),

    /**
     * 文件上传失败
     */
    FILE_UPLOAD_FAILED("fileUploadFailed"),

    /**
     * 文件下载失败
     */
    FILE_DOWNLOAD_FAILED("fileDownloadFailed"),

    ;

    private final String message;

    @Override
    public String message() {
        return this.message;
    }

    @Override
    public String moduleName() {
        return "iking-framework-maintenace-datav";
    }
}
