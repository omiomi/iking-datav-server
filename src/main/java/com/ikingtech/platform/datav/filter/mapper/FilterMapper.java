package com.ikingtech.platform.datav.filter.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.datav.filter.entity.FilterDO;

/**
 * @author fucb
 */
public interface FilterMapper extends BaseMapper<FilterDO> {

}

