package com.ikingtech.platform.datav.filter.model;

import com.ikingtech.framework.sdk.base.model.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author fucb
 */

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@Schema(name = "FilterSearchDTO对象", description = "对象")
public class FilterSearchDTO extends PageParam {


    /**
     * ID
     */
    @Schema(name = "id", description = "ID")
    private String id;

    /**
     * 名称
     */
    @Schema(name = "name", description = "名称")
    private String name;

    /**
     * 项目ID
     */
    @Schema(name = "projectId", description = "项目ID")
    private String projectId;

    /**
     * 屏幕ID
     */
    @Schema(name = "screenId", description = "屏幕ID")
    private String screenId;

    /**
     * 过滤器ID
     */
    @Schema(name = "filterId", description = "过滤器ID")
    private String filterId;

    /**
     * 版本
     */
    @Schema(name = "version", description = "版本")
    private Integer version;

    /**
     * Code
     */
    @Schema(name = "code", description = "Code")
    private String code;

    /**
     * 来源
     */
    @Schema(name = "origin", description = "来源")
    private String origin;

    /**
     * 模板id
     */
    @Schema(name = "templateId", description = "模板id")
    private String templateId;

    /**
     * 过滤器类型（普通，模板）
     */
    @Schema(name = "filterType", description = "过滤器类型（普通，模板）")
    private String filterType;
}

