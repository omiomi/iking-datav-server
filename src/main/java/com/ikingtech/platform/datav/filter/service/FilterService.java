package com.ikingtech.platform.datav.filter.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.framework.sdk.base.model.PageResult;
import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.platform.datav.filter.entity.FilterDO;
import com.ikingtech.platform.datav.filter.exception.FilterExceptionInfo;
import com.ikingtech.platform.datav.filter.mapper.FilterMapper;
import com.ikingtech.platform.datav.filter.model.FilterDTO;
import com.ikingtech.platform.datav.filter.model.FilterSearchDTO;
import com.ikingtech.platform.datav.filter.model.FilterVO;
import com.ikingtech.platform.datav.filter.model.enums.FilterTypeEnum;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

/**
 * @author fucb
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class FilterService extends ServiceImpl<FilterMapper, FilterDO> {

    public PageResult<FilterDO> selectByPage(FilterSearchDTO queryParam) {
        return PageResult.build(this.page(new Page<>(queryParam.getPage(), queryParam.getRows()), Wrappers.<FilterDO>lambdaQuery()
                .like(Tools.Str.isNotBlank(queryParam.getName()), FilterDO::getName, queryParam.getName())
                .eq(Tools.Str.isNotBlank(queryParam.getId()), FilterDO::getId, queryParam.getId())
                .eq(Tools.Str.isNotBlank(queryParam.getProjectId()), FilterDO::getProjectId, queryParam.getProjectId())
                .eq(Tools.Str.isNotBlank(queryParam.getScreenId()), FilterDO::getScreenId, queryParam.getScreenId())
                .eq(Tools.Str.isNotBlank(queryParam.getFilterId()), FilterDO::getFilterId, queryParam.getFilterId())
                .eq(Tools.Str.isNotBlank(queryParam.getTemplateId()), FilterDO::getTemplateId, queryParam.getTemplateId())
                .eq(Tools.Str.isNotBlank(queryParam.getFilterType()), FilterDO::getFilterType, queryParam.getFilterType())
                .eq(Objects.nonNull(queryParam.getVersion()), FilterDO::getVersion, queryParam.getVersion())
                .eq(Tools.Str.isNotBlank(queryParam.getCode()), FilterDO::getCode, queryParam.getCode())));
    }

    public FilterVO getInfo(Integer id) {
        FilterDO entity = this.baseMapper.selectById(id);
        if (null == entity) {
            throw new FrameworkException(FilterExceptionInfo.FILTER_NOT_FOUND);
        }
        return BeanUtil.copyProperties(entity, FilterVO.class);
    }

    @Transactional(rollbackFor = Exception.class)
    public void add(FilterDTO param) {
        FilterDO filter = BeanUtil.copyProperties(param, FilterDO.class);
        filter.setId(IdUtil.simpleUUID());
        filter.setScreenId(Tools.Str.is(param.getScreenId(),null));
        filter.setProjectId(Tools.Str.is(param.getProjectId(),null));
        this.baseMapper.insert(filter);
    }

    public void edit(FilterDTO param) {
        if (!this.exist(param.getId())) {
            throw new FrameworkException(FilterExceptionInfo.FILTER_NOT_FOUND);
        }
        FilterDO filter = BeanUtil.copyProperties(param, FilterDO.class);
        this.baseMapper.updateById(filter);
    }

    public void delete(String id) {
        this.baseMapper.deleteById(id);
    }

    public boolean exist(String id) {
        return this.baseMapper.exists(Wrappers.<FilterDO>lambdaQuery().eq(FilterDO::getId, id));
    }

    public List<FilterDO> getListByScreenId(String screenId) {
        LambdaQueryWrapper<FilterDO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(FilterDO::getScreenId, screenId);
        return this.list(queryWrapper);
    }

    public List<FilterDO> getListByTemplateId(String templateId) {
        LambdaQueryWrapper<FilterDO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(FilterDO::getTemplateId, templateId);
        queryWrapper.eq(FilterDO::getFilterType, FilterTypeEnum.TEMPLATE.name());
        return this.list(queryWrapper);
    }
}
