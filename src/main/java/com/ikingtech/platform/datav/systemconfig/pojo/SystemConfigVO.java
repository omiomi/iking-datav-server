package com.ikingtech.platform.datav.systemconfig.pojo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * created on 2024-03-22 14:43
 *
 * @author wub
 */

@Data
@Schema(name = "SystemConfigDTO", description = "SystemConfigDTO对象")
public class SystemConfigVO {

    @Schema(name = "id")
    private String id;

    @Schema(name = "type", description = "类型")
    private String type;

    @Schema(name = "properties", description = "配置")
    private String properties;
}
