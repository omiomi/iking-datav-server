package com.ikingtech.platform.datav.systemconfig.pojo;

import com.ikingtech.framework.sdk.base.model.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * created on 2024-03-22 14:49
 *
 * @author wub
 */

@EqualsAndHashCode(callSuper = true)
@Data
public class SystemConfigSearchDTO extends PageParam {

    @Schema(name = "id")
    private String id;

    @Schema(name = "type", description = "类型")
    private String type;

    @Schema(name = "properties", description = "配置")
    private String properties;
}
